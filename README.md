![logicicon.svg](http://jw718.user.srcf.net/logicicon.svg) 

GF2 Software Engineering
========================
Team 0
======

Writing a Logic Analyser
------------------------

This is the sourcecode and report work for the team 0 GF2 Software engineering Project.

Team Zero consists of:

*	Alex Read
*	Joseph Warren
*	Hafiz Azman

Build Instructions
------------------

To build, checkout from the repository, and run make in the src/ repository.

Testing
-------

Some gtest (the google test framework) tests have been produced to run these, navigate to the src folder, then to the gtest-1.7.0 folder and run ./configure followed by make. 

Then navigate back to src, and run make test.

To run the tests run ./lexical-analysis-test