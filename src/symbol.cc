#include "symbol.h"

symbol::symbol(){
}

symbol::symbolType symbol::getType(){
  return type;
}

nameSymbol::nameSymbol( name aName ): theName( aName ){
  type = NAME;
}

name nameSymbol::getName(){
  return theName;
}

keywordSymbol::keywordSymbol( keywordType aKeywordType ):
	    theKeywordType( aKeywordType ){
  type = KEYWORD;
}

keywordSymbol::keywordType keywordSymbol::getKeywordType(){
  return theKeywordType;
}

const char * keywordSymbol::KEYWORDS[] = {
    "CLOCK",
    "RCDEVICE",
    "SWITCH",
    "AND",
    "NAND",
    "OR",
    "NOR",
    "DTYPE",
    "XOR",
    "MONITOR",
    "initial",
    "timeconstant",
    "period"
};

valueSymbol::valueSymbol( int aValue ): theValue( aValue ){
  type = VALUE;
}

int valueSymbol::getValue(){
  return theValue;
}

eofSymbol::eofSymbol(){
  type = END_OF_FILE;
}

punctuationSymbol::punctuationSymbol( punctuationType aPunctuationType ):
	    thePunctuationType( aPunctuationType ){
  type = PUNCTUATION;
}

punctuationSymbol::punctuationType punctuationSymbol::getPunctuationType(){
  return thePunctuationType;
}
