#include "logsim.h"
#include "userint.h"
#include "gui.h"
#include "errorlog.h"
#include <GL/glut.h>

#define USE_GUI

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
// This function is automatically called when the application starts
{
  if (argc != 2 && argc !=3 ) { // check we have one command line argument
    wcout << "Usage:      " << argv[0] << " <filename> [en|ms|?]" << endl;
    exit(1);
  }
  int lang = -1;
  if( argc == 3 ){
    if(  wxString(argv[2]) == wxT("en") ){
      lang = 0;
    } else if ( wxString(argv[2]) == wxT("ms") ){
      lang = 1;
    }  else if ( wxString(argv[2]) == wxT("?" ) ){
      lang = -1;
    }
  } else {
  switch( wxLocale::GetSystemLanguage() ){
    case wxLANGUAGE_DEFAULT:
    case wxLANGUAGE_UNKNOWN: 
    case wxLANGUAGE_ENGLISH:
    case wxLANGUAGE_ENGLISH_UK:
    case wxLANGUAGE_ENGLISH_US:
    case wxLANGUAGE_ENGLISH_AUSTRALIA:
    case wxLANGUAGE_ENGLISH_BELIZE:
    case wxLANGUAGE_ENGLISH_BOTSWANA:
    case wxLANGUAGE_ENGLISH_CANADA:
    case wxLANGUAGE_ENGLISH_CARIBBEAN:
    case wxLANGUAGE_ENGLISH_DENMARK:
    case wxLANGUAGE_ENGLISH_EIRE:
    case wxLANGUAGE_ENGLISH_JAMAICA:
    case wxLANGUAGE_ENGLISH_NEW_ZEALAND:
    case wxLANGUAGE_ENGLISH_PHILIPPINES:
    case wxLANGUAGE_ENGLISH_SOUTH_AFRICA:
    case wxLANGUAGE_ENGLISH_TRINIDAD:
    case wxLANGUAGE_ENGLISH_ZIMBABWE:    
      lang = 0;
      break;
    case wxLANGUAGE_MALAY:
      lang = 1;
      break;
    default:
      lang = -1;
    }
  } 

  // Construct the six classes required by the innards of the logic simulator
  nmz = new names();
  netz = new network(nmz);
  dmz = new devices(nmz, netz);
  mmz = new monitor(nmz, netz);
  smz = new scanner(nmz, wxString(argv[1]).mb_str());
  pmz = new parser(netz, dmz, mmz, smz);

  pmz->readin ();
  if (true){ // check the logic file parsed correctly
#ifdef USE_GUI
    // glutInit cannot cope with Unicode command line arguments, so we pass
    // it some fake ASCII ones instead
    char **tmp1 = 0; int tmp2 = 0; glutInit(&tmp2, tmp1);
    // Construct the GUI
    MyFrame *frame = new MyFrame(NULL, wxT("Logic simulator"), lang, wxDefaultPosition,  wxSize(800, 600), nmz, dmz, mmz );
    frame->Show(true);
    return(true); // enter the GUI event loop
#else
    // Construct the text-based interface
    userint umz(nmz, dmz, mmz);
    umz.userinterface();
#endif  /*USE_GUI*/
  } else{
    errorLog& log = errorLog::getInstance();
    cout << "errors: \n";
    for( errorLog::iterator it = log.begin(); it != log.end(); it++ ){
      cout << (*it).getReasonString();
      cout << (*it).getLineString();
    }

  }
  return(false); // exit the application
}
