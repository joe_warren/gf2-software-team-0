#ifndef languages_h
#define languages_h

#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>
#include <wx/tokenzr.h>
#include "names.h"
#include "devices.h"
#include "monitor.h"
#include <vector>
#include <wx/log.h>
#include <cmath>
#include "wx/collpane.h"




class languages{
public:
  static const int MAX_LANGUAGES;

  static const int INPUT_EXCEPTION_OFFSET ;
  enum
  {
    LANG_EN=0,
    LANG_MY=1
  };

  enum
  messages{
    //messages
    MSG_INIT = 0,
    MSG_CANNOT_RUN_CAUSE_ERRORS,
    MSG_MONITOR_POINT_NOT_EXIST,
    MSG_MONITOR_LIMIT,
    MSG_MONITOR_ERROR_REMOVE,
    MSG_NETWORK_OSCILLATING,
    MSG_CYCLE_LIMIT,
    MSG_UNKNOWN_NAME,
    MSG_ERROR_COUNT,
    MSG_ERROR_ON_LINE,

    //menu
    MENU_ABOUT,
    MENU_FILE,
    MENU_QUIT,

    //labels
    LAB_RUN,
    LAB_RESET,
    LAB_NEW_MONITOR,
    LAB_NUMBER_OF_CYCLES,
    LAB_SWITCHES,
    LAB_MONITORS_SLIDE_TOGETHER,
    LAB_REMOVE,

    //misc
    ABOUT_TEXT,
    ABOUT_TITLE,

    //dialog new monitor
    NEWMON_SELECT_OUTPUT,
    NEWMON_MON_POINT



  };

  static const char* en[];

  static const char* my[];

  static const char** langs[];

};




#endif /* languages_h */
