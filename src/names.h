#ifndef names_h
#define names_h

#include <string>
#include <vector>

using namespace std;

typedef int name;

class namestring: public string{
public:
  static const int MAXLENGTH;   /* max chars in a name string   */
public:
  namestring( const char* charAr );
  /* construct a namestring from a string literal, or similar            */

  namestring( string str );
  /* construct a namestring from a string object                         */

  namestring();
  /* default constructor                                                 */
};

class names: public vector<namestring> {

public: 
  static const int MAXNAMES;  /* max number of distinct names */
  static const int BLANKNAME;   /* special name */

public:

  name lookup (namestring str);
  /* Returns the internal representation of the name given in character  */
  /* form.  If the name is not already in the name table, it is          */
  /* automatically inserted.                                             */

  name cvtname (namestring str);
  /* Returns the internal representation of the name given in character  */
  /* form.  If the name is not in the name table then 'blankname' is     */
  /* returned.                                                           */

  void printContents();
  /* Prints the contents of the class, intended for debugging            */

  void writename (name id);
  /* Prints out the given name on the console                            */

  int namelength (name id);
  /* Returns length ie number of characters in given name                */

  names (void);
  /* initialises the name table; default constructor                     */
};

#endif /* names_h */



