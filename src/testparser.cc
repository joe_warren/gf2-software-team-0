i#include <iostream>
#include <cstdlib>
#include <cctype>

#include "parser.h"

network* netz;
names* nmz;
scanner* smz;
devices* dmz;
monitor* mmz;
parser* pmz;

using namespace std;

int main (int argc, char ** argv) {
  if (argc!=2) {
    wcout << "Usage:        " << argv[0] << "[filename]" << endl;
    exit(1);
  }

  nmz = new names();
  netz = new network(nmz);
  dmz = new devices(nmz, netz);
  mmz = new monitor(nmz, netz);
  smz = new scanner(nmz, argv[1]);
  pmz = new parser(netz, dmz, mmz, smz);

  pmz->readin();
}
