
#include "languages.h"
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>
#include <wx/tokenzr.h>
#include "names.h"
#include "devices.h"
#include "monitor.h"
#include <vector>
#include <wx/log.h>
#include <cmath>
#include "wx/collpane.h"



const int languages::MAX_LANGUAGES = 2; 

const int languages::INPUT_EXCEPTION_OFFSET = 24; 

const char* languages::en[] = {
    "Logsim Initialized . . .",
    "Cannot run due to errors; check this log for details",
    "Error: unable to set monitor point. Does the point exist?",
    "Monitor Limit Reached",
    "Error removing monitor.",
    "Error: network is oscillating.",
    "Cycle limit reached; press reset to continue.",
    "Error: unknown name",
    "%d errors",
    "An error occured on line ",

    //labels

    //menu
    "&About",
    "&File",
    "&Quit",

    //labels
    "Run",
    "Reset",
    "New Monitor..",
    "Number of Cycles",
    "Switches",
    "Monitors Slide Together",
    "Remove",

    //misc
    "Team Zero Logic Simulator \n \n Alex Read \n Joe Warren \n Hafiz Azman \n \n \n 2014",
    "Team Zero Logic Simulator",

    //dialog new monitor
    "Select output..",
    "New Monitor",

    // THIS POINT SHOULD BE KEPT AS INPUT EXCEPTION OFFSET
    "This reason has been entered in error",
    "A semicolon was expected, and wasn't found",
    "An invalid type name was found",
    "An invalid device name was found",
    "An invalid property name was found",
    "An invalid input name was found",
    "more inputs were expected",
    "A keyword was used as a device name",
    "A open brace was expected, and was not found",
    "A close brace was expected, and was not found",
    "An equals sign was expected, and was not found",
    "An input was used which is not appropriate for this device",
    "The same device name was used in multiple places",
    "An output was referred to which is non existent",
    "A device was referred to which is non existent",
    "A property that was expected is missing",
    "An invalid property was found",
    "Too many monitoring points - the maximum is 10",
    "A device name was expected, but was not found",
    "Two inputs with the same name were found"

};


const char* languages::my[] = {
    "Logsim dimulakan.",
    "Program tidak boleh jalan. Sila bacakan kotak teks di bawah untuk mendapat tahu lebih.",
    "Tempat yang diperhatikan tidak didapati. Tempat ini ada ke?",
    "Sudah sampai had.",
    "Pemerhati tidak boleh dikeluarkan.",
    "Rangkaian berayun tak henti-henti!",
    "Sudah sampai had. Sila tekan 'Reset'",
    "Ups silap! Nama tak ada.",
    "%d kesilapan",
    "Kesilapan ada di barisan ",

    //labels

    //menu
    "&Tentang",
    "&Fail",
    "&Berhenti",

    //labels
    "Jalan",
    "Jalan Semula",
    "Pemerhati Baru..",
    "Bilangan Kitaran",
    "Suis",
    "Pemerhati Jalan Bersama-sama",
    "Keluarkan",

    //misc
    "Pasukan Kosong Simulasi Logik \n"
    "\n"
    "\xE4\xBA\x9A\xE5\x8E\x86\xE5\x85\x8B\xE6\x96\xAF\xE9\x98\x85\xE8\xAF\xBB"
    "\n"
    "\xE4\xB9\x94\xC2\xB7\xE6\xB2\x83\xE4\xBC\xA6 "
    "\n"
    "\xE5\x93\x88\xE8\x8F\xB2\xE5\x85\xB9\xE9\x98\xBF\xE5\x85\xB9\xE6\x9B\xBC"
    "\n \n \n 2014",
    "Pasukan Kosong Simulasi Logik",

    //dialog new monitor
    "Pilih Output",
    "Pemerhati Baru",

    // THIS POINT SHOULD BE KEPT AS INPUT EXCEPTION OFFSET
    "Sebab ini dituliskan tidak betul",
    "Tidak jumpa koma bertitik",
    "Nama jenis tidak sah",
    "Nama peranti tidak sah",
    "Nama properti tidak sah",
    "Nama input tak sah!",
    "Kita memerlukan lebih input",
    "Nama peranti digunakan sebagai kata kunci",
    "Pendakap terbuka dijangka, tetapi tidak ditemui",
    "Pendakap tertutup dijangka, tetapi tidak ditemui",
    "Tanda sama dijangka; tidak ditemui",
    "Input tidak sesuai untuk peranti!",
    "Nama peranti digunakan dua kali atau lebih.",
    "Output yang dipanggil tak ada",
    "Peranti yang dipanggil tak ada",
    "Properti yang dipanggil tak ada",
    "Properti tak sah",
    "Terlalu banyak pemerhati! Maksimum hanya sepuluh.",
    "Nama peranti dijangka, tetapi tidak ada.",
    "Dua input dengan nama yang sama telah dijumpai"
};




const char** languages::langs[] = {languages::en,languages::my};
