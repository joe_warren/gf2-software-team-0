#ifndef gui_h
#define gui_h
#include <wx/wx.h>

#include <wx/glcanvas.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>
#include <wx/tokenzr.h>
#include "names.h"
#include "devices.h"
#include "monitor.h"
#include <vector>
#include <wx/log.h>
#include <cmath>
#include "wx/collpane.h"
#include "languages.h"

enum { 
  MY_SPINCNTRL_ID = wxID_HIGHEST + 1,
  MY_TEXTCTRL_ID,
  MY_BUTTON_ID,
}; // widget identifiers

class MyGLCanvas;

class MyFrame: public wxFrame
{
public:
  wxString enumToWxString (languages::messages the_enum);
  MyFrame(wxWindow *parent, const wxString& title, int lang, const wxPoint& pos, const wxSize& size, 
      names *names_mod = NULL, devices *devices_mod = NULL, monitor *monitor_mod = NULL,
      long style = wxDEFAULT_FRAME_STYLE); // constructor
  int cyclescompleted;                    // how many simulation cycles have been completed

  int current_language;

  void updateGLSliders( int pos );
private:
  bool cmdok;
  MyGLCanvas *canvas;                     // GL drawing area widget to draw traces


  /*Experimental*/
  vector<wxBoxSizer*> arrPaneBoxSizers;
  vector<MyGLCanvas*> arrCanvas;
  vector<wxButton*> arrDeleteButtons;
  vector<wxStaticText*> arrMonLabels;

  /*Box Sizers*/

  wxBoxSizer *mainBoxSizer;
  wxBoxSizer *rowOneBS;
  wxBoxSizer *rowOneLeftBS;
  wxBoxSizer *rowTwoBS;
  wxBoxSizer *rowTwoPointFiveBS;
  wxBoxSizer *rowThreeBS;
  wxBoxSizer *rowFourBS;


  wxCheckBox* slideTogetherCB;

  wxSpinCtrl *spin;      // control widget to select the number of cycles
  wxSlider *sliderCycle;
  wxLogTextCtrl *txtTarget;
  wxCheckListBox *switchList;
  names *nmz;                             // pointer to names class
  devices *dmz;                           // pointer to devices class
  monitor *mmz;                           // pointer to monitor class

  void addInitialErrors();           // function to add initial errors
  void runnetwork(int ncycles);           // function to run the logic network
  void populateSwitches();           // function to run the logic network
  void populateMonitors();           // function to run the logic network
  void OnExit(wxCommandEvent& event);     // callback for exit menu item
  void OnAbout(wxCommandEvent& event);    // callback for about menu item
  void OnRunButton(wxCommandEvent& event);   // callback for push button
  void OnResetButton(wxCommandEvent& event);   // callback for push button
  void OnCheckListBox(wxCommandEvent& event);   // callback for push button
  void OnAddMonitorButton(wxCommandEvent& event);   // callback for push button
  void OnRemoveMonitorButton(wxCommandEvent& event);   // callback for push button
  void OnSwitchChange(wxCommandEvent& event);   // callback for push button
  void OnSliderChange(wxScrollEvent& event);   // callback for slider
  name namestringToName(namestring ns);
  namestring nameToNamestring(name n);
  DECLARE_EVENT_TABLE()
};

//FIZZZZZZZ

class FlexGridSizer : public wxFrame
{
public:
  FlexGridSizer(const wxString& title);

};

class MyGLCanvas: public wxGLCanvas
{
public:
  MyGLCanvas(wxWindow *parent, 
      wxWindowID id = wxID_ANY, monitor* monitor_mod = NULL, names* names_mod = NULL,
      const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0,
      const wxString& name = wxT("MyGLCanvas")); // constructor
  void Render(int cycles = -1); // function to draw canvas contents
  void setMonitorNum(int monnum);
   void setSlider( int pos );
private:
   void InitGL();                     // function to initialise GL context
   void OnSize(wxSizeEvent& event);   // callback for when canvas is resized
   void OnPaint(wxPaintEvent& event); // callback for when canvas is exposed
   void OnScroll(wxScrollWinEvent& event);   // callback for when canvas is resized
   void OnShow(wxShowEvent& event);     // callback for activation

public:
  bool hasBeenDrawn;
  int cyclesdisplayed;               // how many simulation cycles have been displayed
private:
  static const int PERIOD;
  int height;
  int width;
  int monnum;
  bool init;                         // has the GL context been initialised?
  monitor *mmz;                      // pointer to monitor class, used to extract signal traces
  names *nmz;                        // pointer to names class, used to extract signal names
  MyFrame * theParent;

  DECLARE_EVENT_TABLE()
};

#endif /* gui_h */
