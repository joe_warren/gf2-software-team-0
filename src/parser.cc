#include <iostream>
#include "parser.h"

using namespace std;

/* The parser for the circuit definition files */

/* getPeriod() is called for clocks during the first pass, to find their period. It starts on the name symbol, so 
   steps though the open brace symbol, the period symbol, the equals symbol, the value symbol, the semi-colon symbol
   and the close brace symbol. The only unknown (assuming correct syntax & semantics), is the period value, so the
   rest of the funcion is just a check on the correctness of the file. It returns any value from the valueSymbol, 
   even one that is not a vaid period. It's return value should be checked by the caller  */
int parser::getPeriod() {

  int period=-1;
  bool retVal=true;
  symbol* currentSymbol;

  currentSymbol = &smz->getNextSymbol();


  if (!isOpenBrace(currentSymbol)) {
    retVal=false;
    throw inputException(inputException::MISSING_OPEN_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();


  if(currentSymbol->getType()==symbol::KEYWORD
      && static_cast<keywordSymbol*>(currentSymbol)->getKeywordType()==keywordSymbol::PERIOD){
    currentSymbol=&smz->getNextSymbol();
  }
  else {
    retVal=false;
    throw inputException(inputException::INVALID_PROPERTY_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(!isEquals(currentSymbol)) {
    retVal=false;
    throw inputException(inputException::MISSING_EQUALS, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if(currentSymbol->getType() != symbol::VALUE) {
    retVal = false;
    throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(retVal) {
    period=static_cast<valueSymbol*>(currentSymbol)->getValue();
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isSemiColon(currentSymbol)) { 
    retVal=false;
    throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isCloseBrace(currentSymbol)) {
    retVal=false;
    throw inputException(inputException::MISSING_CLOSE_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  return period;
}

/* getInitial() is very similar to getPeriod, so follows the same rules and same form. Like for getPeriod, it returns
   any value stored in the valueSymbol, even if it is not valid for switches. That is checked by the caller. */
int parser::getInitial() {
  int initial=-1;
  bool retVal=true;
  symbol* currentSymbol;

  currentSymbol = &smz->getNextSymbol();
  if (!isOpenBrace(currentSymbol)) {
    throw inputException(inputException::MISSING_OPEN_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();
  if(currentSymbol->getType()==symbol::KEYWORD && 
      static_cast<keywordSymbol*>(currentSymbol)->getKeywordType()==keywordSymbol::INITIAL){
    currentSymbol=&smz->getNextSymbol();
  }
  else {
    throw inputException(inputException::INVALID_PROPERTY_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(!isEquals(currentSymbol)) {
    throw inputException(inputException::MISSING_EQUALS, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if(currentSymbol->getType() != symbol::VALUE) {
    throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(retVal) {
    initial=static_cast<valueSymbol*>(currentSymbol)->getValue();
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isSemiColon(currentSymbol)) { 
    throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isCloseBrace(currentSymbol)) { 
    throw inputException(inputException::MISSING_CLOSE_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  return initial;
}

/* getTimeConstant() is very similar to getPeriod, so follows the same rules and same form. Like for getPeriod, it returns
   any value stored in the valueSymbol, even if it is not valid for switches. That is checked by the caller. */
int parser::getTimeConstant() {
  int initial=-1;
  bool retVal=true;
  symbol* currentSymbol;

  currentSymbol = &smz->getNextSymbol();
  if (!isOpenBrace(currentSymbol)) {
    retVal=false;
    throw inputException(inputException::MISSING_OPEN_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();
  if(currentSymbol->getType()==symbol::KEYWORD && 
      static_cast<keywordSymbol*>(currentSymbol)->getKeywordType()==keywordSymbol::TIMECONSTANT){
    currentSymbol=&smz->getNextSymbol();
  }
  else {
    retVal=false;
    throw inputException(inputException::INVALID_PROPERTY_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(!isEquals(currentSymbol)) {
    retVal=false;
    throw inputException(inputException::MISSING_EQUALS, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if(currentSymbol->getType() != symbol::VALUE) {
    retVal = false;
    throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(retVal) {
    initial=static_cast<valueSymbol*>(currentSymbol)->getValue();
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isSemiColon(currentSymbol)) { 
    retVal=false;
    throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  currentSymbol = &smz->getNextSymbol();
  if (!isCloseBrace(currentSymbol)) { 
    retVal=false;
    throw inputException(inputException::MISSING_CLOSE_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  return initial;
}



/* The first pass of the parser extracts the types and names of each device, as well as the properties of those where
   appropriate, and the number of inputs for gates other than XOR gates. It uses the private member method 
   int countSemiColonsInBlock() to skip to the end of a block, even if the number of semicolons is not recorded  */
bool parser::firstPass() {
  symbol* currentDeviceTypeSymbol;
  keywordSymbol* currentDeviceTypeKeyword = NULL;
  symbol* currentDeviceNameSymbol;
  nameSymbol* currentDeviceName;
  int deviceVariable = -1;
  devicekind kind;
  bool retVal=true;
  //For all devices in the list...
  while (true) {
    try{
      currentDeviceTypeSymbol = &smz->getNextSymbol();

      //File has ended after a device - success!
      if (currentDeviceTypeSymbol->getType()==symbol::END_OF_FILE) {
        return retVal;
      }
      //We have what could be a new device type
      else if (currentDeviceTypeSymbol->getType()==symbol::KEYWORD) {
        currentDeviceTypeKeyword = static_cast<keywordSymbol*> (currentDeviceTypeSymbol);
      }
      //We do not have a keyword, but did expect one
      else {
        retVal=false;
        throw inputException(inputException::INVALID_TYPE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
      }


      currentDeviceNameSymbol = &smz->getNextSymbol();
      //Check the device type is followed by a name, and store it as a name
      if (currentDeviceNameSymbol->getType() == symbol::NAME) {
        currentDeviceName = static_cast<nameSymbol*> (currentDeviceNameSymbol);
      }
      //or there's an error
      else {
        retVal=false;
        throw inputException(inputException::INVALID_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
      }
      if( currentDeviceTypeKeyword != NULL ){
        //Find out what device we are dealing with, and extract appropriate information, and move to close brace
        switch (currentDeviceTypeKeyword->getKeywordType()){

          case keywordSymbol::RCDEVICE:
            deviceVariable = getTimeConstant();
            if(deviceVariable<1) {
              retVal=false;
              throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
            }
            kind = RCDEVICE;
            break;
          case keywordSymbol::CLOCK:
            deviceVariable = getPeriod();
            if(deviceVariable<1) {
              retVal=false;
              throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
            }
            kind = ACLOCK;
            break;
          case keywordSymbol::SWITCH:
            deviceVariable = getInitial();
            if(deviceVariable!= 0 && deviceVariable !=1) {
              retVal=false;
              throw inputException(inputException::INVALID_PROPERTY_VALUE, smz->getPositionInFile(), smz->getCurrentLine() );
            }
            kind=ASWITCH;
            break;
          case keywordSymbol::AND:
            kind=ANDGATE;
            deviceVariable=countSemiColonsInBlock();
            break;
          case keywordSymbol::NAND:
            kind=NANDGATE;
            deviceVariable=countSemiColonsInBlock();
            break;
          case keywordSymbol::OR:
            kind=ORGATE;
            deviceVariable=countSemiColonsInBlock();
            break;
          case keywordSymbol::NOR:
            kind=NORGATE;
            deviceVariable=countSemiColonsInBlock();
            break;
          case keywordSymbol::XOR:
            kind=XORGATE;
            deviceVariable=2;
            if(countSemiColonsInBlock()!=2) {
	      throw(inputException(inputException::MISSING_INPUT, smz->getPositionInFile(), smz->getCurrentLine() ));
	    }
            break;
          case keywordSymbol::DTYPE:
            kind=DTYPE;
            if(countSemiColonsInBlock()!=4) {
	      throw(inputException(inputException::MISSING_INPUT, smz->getPositionInFile(), smz->getCurrentLine() ));
	    }
            break;
          case keywordSymbol::MONITOR:
            if(countSemiColonsInBlock()>10) {
              throw inputException(inputException::TOO_MANY_MONITORS, smz->getPositionInFile(), smz->getCurrentLine() );
              retVal=false;
            }

            continue;
          default:
            kind=BADDEVICE;
            break;
        };


        dmz->makedevice(kind, currentDeviceName->getName(), deviceVariable, retVal);
      }
      if(!retVal) {
        throw inputException(inputException::INVALID_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
      }
      cout<<endl;
    }catch (inputException e) {
      errorLog &log = errorLog::getInstance();
      log.push_back(e);
      countSemiColonsInBlock();
      retVal = false;

    }
    currentDeviceTypeKeyword = NULL;
  }

  return retVal;
}

bool parser::isSpecificPunctuationType( symbol* aSymbol, 
    punctuationSymbol::punctuationType aType ){

  if( aSymbol->getType() != symbol::PUNCTUATION ){
    return false;
  }
  punctuationSymbol *ps = static_cast<punctuationSymbol *> (aSymbol);

  if( ps->getPunctuationType() != aType ){
    return false;
  }
  return true;
}

bool parser::isOpenBrace (symbol* aSymbol){
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::OPEN_BRACE );
}
bool parser::isCloseBrace (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::CLOSE_BRACE );
}
bool parser::isSemiColon (symbol* aSymbol){
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::SEMICOLON );
}
bool parser::isStop (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::STOP );
}
bool parser::isEquals (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::EQUALS );
}
bool parser::isDeviceType (symbol* aSymbol) {
  if( aSymbol->getType() != symbol::KEYWORD ){
    return false;
  }
  keywordSymbol *ks = static_cast<keywordSymbol *> (aSymbol);

  if( ks->getKeywordType() >= keywordSymbol::CLOCK && ks->getKeywordType() <= keywordSymbol::MONITOR ){
    return true;
  }
  return false;
}

/* takes the keywordSymbol containing the device type and makes it the appropriate devicekind (also a parameter), 
   necessary for making a connection to the input of a device.
 */

bool parser::typeName(symbol* currentSymbol, devicekind& kind) {

  if(currentSymbol->getType()!= symbol::KEYWORD) {
    throw inputException(inputException::INVALID_TYPE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
    return false;
  }

  keywordSymbol* currentDeviceTypeKeyword = static_cast <keywordSymbol*> (currentSymbol);
  switch (currentDeviceTypeKeyword->getKeywordType()){
    case keywordSymbol::CLOCK:
      kind = ACLOCK;
      break;
    case keywordSymbol::RCDEVICE:
      kind=RCDEVICE;
      break;
    case keywordSymbol::SWITCH:
      kind=ASWITCH;
      break;
    case keywordSymbol::AND:
      kind=ANDGATE;
      break;
    case keywordSymbol::NAND:
      kind=NANDGATE;
      break;
    case keywordSymbol::OR:
      kind=ORGATE;
      break;
    case keywordSymbol::NOR:
      kind=NORGATE;
      break;
    case keywordSymbol::XOR:
      kind=XORGATE;
      break;
    case keywordSymbol::DTYPE:
      kind=DTYPE;
      break;
    case keywordSymbol::MONITOR:
      kind=MONITORDEVICE;
      break;
    default:
      kind=BADDEVICE;
      throw inputException(inputException::INVALID_TYPE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
      return false;
  }
  return true;
}


/* input () makes a connection from the specified input of the current device (passed in devName) and the output
   of the device after the equals. If the input name is not the name of a legitimate input, then retVal will be
   set to false by the makeconnection method of network. It detects if there is a stop or a semicolon after the 
   output device to determine if it has more than one potential output.  */
bool parser::input (symbol* currentSymbol, name devName, vector<name>& inputnames) {
  //input = input_name "=" output_id
  //output_id = device_name[.output_name]
  name inputName;
  name outputDeviceName;
  name outputName;
  bool retVal=true;

  //check if the first thing encountered is a name. If it is, store it as the input name.
  if(currentSymbol->getType()!=symbol::NAME) {
    throw inputException(inputException::INVALID_TYPE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  else {
    inputName = static_cast<nameSymbol*>(currentSymbol)->getName();
  }
  currentSymbol = &smz->getNextSymbol();

  if(!isEquals(currentSymbol)) {
    throw inputException(inputException::MISSING_EQUALS, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();

  //get the output devices name, as above
  if(currentSymbol->getType()!=symbol::NAME) {
    throw inputException(inputException::INVALID_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  else {
    outputDeviceName = static_cast<nameSymbol*>(currentSymbol)->getName();
  }
  currentSymbol = &smz->getNextSymbol();

  //check if the output is a compound name, or just the device name
  if(isSemiColon(currentSymbol)) {
    outputName = names::BLANKNAME;
  }
  else if(isStop(currentSymbol)) {
    currentSymbol = &smz->getNextSymbol();
    outputName = static_cast<nameSymbol*>(currentSymbol)->getName();

    //Check line ends with a semicolon
    currentSymbol = &smz->getNextSymbol();
    if(!isSemiColon(currentSymbol)) {
      throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
    }
  }
  else {
    throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  if( inputnames.end() != find( inputnames.begin(), inputnames.end(), inputName ) ){
    throw inputException(inputException::DUPLICATE_INPUT_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  inputnames.push_back( inputName );

  netz->makeconnection(devName, inputName, outputDeviceName, outputName, retVal); 
  return retVal;
}


/* monitorInput() is very similar to input(), but it adds a monitor rather than making a connection. Because the high
   level design chose to make the monitor act like a device but at a low level it acts very differently, this function 
   is designed to appear like input() from the outside. 
 */
bool parser::monitorInput(symbol* currentSymbol) {

  bool retVal=true;
  //  name inputName;
  name outputDeviceName;
  name outputName;

  //check if the first thing encountered is a name. If it is, store it as the input name.
  if(currentSymbol->getType()!=symbol::NAME) {
    throw inputException(inputException::INVALID_INPUT_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();

  if(!isEquals(currentSymbol)) {
    throw inputException(inputException::MISSING_EQUALS, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  currentSymbol = &smz->getNextSymbol();

  //get the output devices name, as above
  if(currentSymbol->getType()!=symbol::NAME) {
    throw inputException(inputException::INVALID_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
  }
  else {
    outputDeviceName = static_cast<nameSymbol*>(currentSymbol)->getName();
  }
  currentSymbol = &smz->getNextSymbol();

  //check if the output is a compound name, or just the device name - i.e. is it a dtype?
  if(isSemiColon(currentSymbol)) {
    outputName = names::BLANKNAME;
  }
  else if(isStop(currentSymbol)) {
    currentSymbol = &smz->getNextSymbol();
    outputName = static_cast<nameSymbol*>(currentSymbol)->getName();

    //Check line ends with a semicolon
    currentSymbol = &smz->getNextSymbol();
    if(!isSemiColon(currentSymbol)) {
      throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
    }
  }
  else {
    throw inputException(inputException::MISSING_SEMICOLON, smz->getPositionInFile(), smz->getCurrentLine() );
  }

  if(retVal) {
    mmz->makemonitor(outputDeviceName, outputName, retVal);
  }
  return retVal;
}

/* device() is the main method during the second pass of the parser - the file is a list of devices, so the second pass
   loops over this method until the end of file is reached. Although EBNF allows any parameter within the braces, this
   function ignores properties, as they are read on the first pass by the parser.
 */
bool parser::device(symbol* currentSymbol, vector<name>& devicenames ) {
  //device = type_name device_name  "{" {parameter} "} ;"

  bool retVal=true;
  devicekind devKind;
  name devName;
  try{

    //check device starts with a type_name, and find out what type of device it is
    if(!typeName(currentSymbol, devKind)) {
      throw inputException(inputException::INVALID_TYPE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
    }
    currentSymbol = &smz->getNextSymbol();

    //check that this is followed by a device_name
    if(currentSymbol->getType() == symbol::NAME) {
      devName=static_cast<nameSymbol*>(currentSymbol)->getName();
    }
    else {
      throw inputException(inputException::MISSING_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
    }

    if( devicenames.end() != find( devicenames.begin(), devicenames.end(), devName ) ){
      throw inputException(inputException::DUPLICATE_DEVICE_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
    }
    devicenames.push_back( devName );

    currentSymbol = &smz->getNextSymbol();

    if (!isOpenBrace(currentSymbol)) {
      throw inputException(inputException::MISSING_OPEN_BRACE, smz->getPositionInFile(), smz->getCurrentLine() );
    }
    currentSymbol = &smz->getNextSymbol();

    //clocks and switches have no inputs, monitor is done slightly differently
    if( !(  devKind == ASWITCH 
         || devKind == RCDEVICE
         || devKind == ACLOCK
         || devKind == MONITORDEVICE
         || devKind == BADDEVICE ) ) { 
      //Anything with an input needs to have outputs connected to them
      vector<name> inputnames;
      while (!isCloseBrace(currentSymbol)&& (currentSymbol->getType()!=symbol::END_OF_FILE)) {
        if(!input(currentSymbol, devName, inputnames)) {
          retVal = false;
          throw inputException(inputException::INVALID_INPUT_NAME, smz->getPositionInFile(), smz->getCurrentLine() );
        }
        currentSymbol = &smz->getNextSymbol();
      }
    }
    else if(devKind==MONITORDEVICE) {
      while (!isCloseBrace(currentSymbol) && currentSymbol->getType()!=symbol::END_OF_FILE) {
        if(!monitorInput(currentSymbol)) {
          retVal = false;
          throw inputException(inputException::UNEXISTENT_OUTPUT, smz->getPositionInFile(), smz->getCurrentLine() );
        }
        currentSymbol = &smz->getNextSymbol();
      }
    }
    else if ((devKind==ASWITCH) || (devKind == RCDEVICE) || (devKind==ACLOCK) ) {
      countSemiColonsInBlock();


    }
  }catch(inputException e) {
    retVal = false;
    errorLog &log = errorLog::getInstance();
    log.push_back(e);
    countSemiColonsInBlock();
  }
  return retVal;
}

int parser::countSemiColonsInBlock(){
  int i = 0;
  symbol* nextTerminator = &( smz->skipUntilTerminator() );	

  while( isSemiColon( nextTerminator )){
    i++;
    nextTerminator = &( smz->skipUntilTerminator() );	
  }

  return i;
}

/* The second pass loop - much shoter than the first, because of the simplicity of the top level of the
   EBNF: a series of devices. 
 */

bool parser::secondPass() {
  bool retval = true;
  cout << endl;

  symbol* currentSymbol=&smz->getNextSymbol();
  vector<name> devicenames;
  while (currentSymbol->getType() != symbol::END_OF_FILE) {
    if(!device(currentSymbol, devicenames)){
      retval = false;
    }
    currentSymbol=&smz->getNextSymbol();
  }
  return retval;
}

/* Calls the first pass, then rewinds the scanner, then calls the second pass. Returns true if both passes 
   retun true (i.e. no errors)  */
bool parser::readin (void){
  if(! firstPass()) {
    return false;
  }
  smz->rewind();
  if(!secondPass()) {
    return false;
  }
  return true;
}

parser::parser (network* network_mod, devices* devices_mod,
    monitor* monitor_mod, scanner* scanner_mod)
{
  netz = network_mod;  /* make internal copies of these class pointers */
  dmz = devices_mod;   /* so we can call functions from these classes  */
  mmz = monitor_mod;   /* eg. to call makeconnection from the network  */
  smz = scanner_mod;   /* class you say:                               */
}







